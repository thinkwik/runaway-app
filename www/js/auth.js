var authCtrl = angular.module('run-away.authControllers', []);

authCtrl.controller('LoginCtrl', function ($scope, $rootScope, $ionicSideMenuDelegate, $timeout, $ionicPopup, $ionicModal, $stateParams, $state)
{
    $scope.login = function()
    {
        $rootScope.showIntro = true;
        $state.go("tab.dash");
    }
});

authCtrl.controller('LostPasswordCtrl', function ($scope) {

});

authCtrl.controller('signUpCtrl', function ($scope, $rootScope)
{
    $scope.signup = function()
    {
        $rootScope.showIntro = true;
        $state.go("tab.dash");
    }
});