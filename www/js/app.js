angular.module('run-away', ['ionic', 'run-away.controllers', 'run-away.services', 'ngMaterial', 'run-away.homeController', 'run-away.authControllers'])

.run(function($ionicPlatform, $rootScope, $ionicHistory)
{
    $rootScope.showIntro = false;
    $rootScope.goBack = function()
    {
        $ionicHistory.goBack();
    }

    $ionicPlatform.ready(function ()
    {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard)
        {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar)
        {
            StatusBar.styleDefault();
        }
    });
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider)
{
    $ionicConfigProvider.views.transition("ios");
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.form.checkbox("circle");
    $ionicConfigProvider.tabs.position("bottom");
    $ionicConfigProvider.navBar.alignTitle("left");
    $ionicConfigProvider.tabs.style("striped");

    $stateProvider
    .state('home', {
        url: "/home",
        templateUrl: "templates/home.html",
        controller: 'HomeCtrl'
    })
    .state('login', {
        url: "/login",
        templateUrl: "templates/auth/login.html",
        controller: 'LoginCtrl'
    })
    .state('signup', {
        url: "/signup",
        templateUrl: "templates/auth/signup.html",
        controller: 'signUpCtrl'
    })
    .state('lostpassword', {
        url: "/lostpassword",
        templateUrl: "templates/auth/lostpassword.html",
        controller: 'LostPasswordCtrl'
    })
    .state('list', {
        url: "/list",
        templateUrl: "templates/list.html",
        controller: 'ListCtrl'
    })
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })
    .state('tab.dash', {
        url: '/dash',
        views: {
            'tab-dash': {
                templateUrl: 'templates/tab-dash.html',
                controller: 'DashCtrl'
            }
        }
    })
    .state('tab.chats', {
        url: '/chats',
        views: {
            'tab-chats': {
                templateUrl: 'templates/tab-chats.html',
                controller: 'DashCtrl'
            }
        }
    })
    .state('tab.chat-detail', {
        url: '/chats/:chatId',
        views: {
            'tab-chats': {
                templateUrl: 'templates/chat-detail.html',
                controller: 'DashCtrl'
            }
        }
    })
    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                templateUrl: 'templates/tab-account.html',
                controller: 'DashCtrl'
            }
        }
    });
    $urlRouterProvider.otherwise('/home');
});