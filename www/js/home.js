var homeCtrl = angular.module('run-away.homeController', []);

homeCtrl.controller('HomeCtrl', function ($scope, $ionicSideMenuDelegate, $timeout, $ionicPopup, $ionicModal, $stateParams)
{
    $scope.LostPasswordModal = function ()
    {
        $scope.lostpasswordmodelview.show();
    };

    $scope.closelostpasswordModal = function ()
    {
        $scope.lostpasswordmodelview.hide();
    };

    $ionicModal.fromTemplateUrl('templates/auth/lostpassword.html', {
        scope: $scope,
        animation: 'animated bounceInDown'
    }).then(function (modal)
    {
        $scope.lostpasswordmodelview = modal;
    });

    $scope.myDate = new Date();
});