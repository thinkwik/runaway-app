var appCtrl = angular.module('run-away.controllers', []);

appCtrl.controller('DashCtrl', function ($scope, $rootScope, $ionicModal, $timeout, $ionicSlideBoxDelegate)
{
    $scope.slideIndex = 0;

    $scope.goToDashboard = function()
    {
        $rootScope.showIntro = false;
    }

    $scope.nextSlide = function ()
    {
        $ionicSlideBoxDelegate.next();
    }

    $scope.slideChanged = function (index)
    {
        $scope.slideIndex = index;
    };
})

appCtrl.controller('ListCtrl', function ($scope) {


});